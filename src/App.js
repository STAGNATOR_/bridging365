import "./App.css";
import Form from "./page/form/form";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./page/homePage/homePage";
import { NonIvestment } from "./page/homePage/nonIvestment";
import Dashboard from "./page/adminDashboard/dashboard";
import { useState } from "react";

function App() {
  const [id, setId] = useState("");
  console.log("id", id);
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Form setId={setId} />} />
        </Routes>
        <Routes>
          <Route path="/home-page" element={<HomePage id={id} />} />
        </Routes>
        <Routes>
          <Route path="/non-investment" element={<NonIvestment />} />
        </Routes>
        <Routes>
          <Route path="/dashboard" element={<Dashboard />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
