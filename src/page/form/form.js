import { useEffect, useState } from "react";
import axiosClient from "../../helper";
import { Button, Container, Grid, TextField, Typography } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import MuiPhoneNumber from "material-ui-phone-number";

import classes from "./form.module.css";

import birdimg from "../../Assets/bird-red.gif";
import headimg from "../../Assets/header.png";
import infoimg from "../../Assets/info.png";
import { Link, useNavigate } from "react-router-dom";
const Form = (props) => {
  
  const navigate = useNavigate();
  const [form, setForm] = useState([
    {
      id: 0,
      question: "What is your full name?",
      answer: "",
      placeHolder:"...",
      answerOptions: "",
      questionType: "singleInput",
    },
    {
      id: 1,
      question: "What is the best number to contact you on?",
      answer: "",
      answerOptions: "",
      questionType: "phone",
    },
    {
      id: 2,
      question: "And your email address?",
      answer: "",
      placeHolder:"...",
      answerOptions: "",
      questionType: "singleInput",
    },
    {
      id: 3,
      question: "Will you live in the property or is it an investment?",
      answer: "",
      answerOptions: ["It is an investment", "I'll be living in the property"],
      questionType: "boolean",
    },
    {
      id: 4,
      question: "How quickly do you need the money?",
      answer: "",
      answerOptions: [
        "Urgently within days",
        "Required within a few weeks but less than 3 months",
      ],
      questionType: "boolean",
    },
  ]);

  function handlePhoneChange(value, type) {
    console.log("Value", value);
    const previousArray = [...form];
    let objIndex = previousArray.findIndex((obj) => obj.questionType === type);
    previousArray[objIndex].answer = value;
    setForm(previousArray);
  }
  const PostDataHandler = () => {
    let data = {
      home: form,
    };
    axiosClient()
      .post(`mortage/createMortgage`, data)
      .then((res) => {
        props.setId(res.data.data._id);
        console.log("api response ", res.data.data._id);
        navigate("/home-page", {
          state:{
            divId:"123"
          }
        });
      });
  };
  function handleChange(e, id) {
    const { value } = e.target;
    console.log("id", id);
    console.log("e.target", e.target);
    console.log("value", value);

    const previousArray = [...form];

    //Find index of specific object using findIndex method.
    let objIndex = previousArray.findIndex((obj) => obj.id == id);

    //Log object to Console.
    console.log("Before update: ", previousArray[objIndex]);

    //Update object's name property.
    previousArray[objIndex].answer = value;

    //Log object to console again.
    console.log("After update: ", previousArray[objIndex]);
    setForm(previousArray);
  }
  return (
    <>
      <Container>
        <form>
          <Grid container sx={{ justifyContent: "center" }}>
            <Grid sx={{ textAlign: "center" }}>
              <img src={headimg} alt="header image" width="20%" />

              <Typography className={classes.homepage_welcome_text}>
                
              </Typography>
              <Typography className={classes.homepage_mortgage_text}>
                Online Bridging Loan Application
              </Typography>

              <Grid lg={6} conatiner className={classes.gifCard}>
                <ul style={{ textAlign: "start" }}>
                  <li className={classes.calculator}>
                    <Typography>
                    You will be able to access the best market-leading bridging rates on offer 
                    </Typography>
                  </li>
                  <li className={classes.certificate}>
                    <Typography>
                    The application will take around 10 minutes and you will receive a personalised Decision in Principle (DIP)
                    </Typography>
                  </li>
                  <li className={classes.euro}>
                    <Typography>The DIP will tell you how much it will cost to borrow the funds, and how quickly you can receive the money. Let's begin with some basic information...
</Typography>
                  </li>
                </ul>
              <img src={birdimg} alt="bird" className={classes.bird} />

              </Grid>
            </Grid>
            {form.map((x) => (
              <>
                <Grid
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    marginBottom: "2rem",
                    marginTop: "2rem",
                  }}
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                >
                  <Grid
                    className={classes.secGifCard}
                    sx={{
                      display: "flex",
                      justifyContent: "start",
                      alignItems: "center",
                    }}
                  >
                    <Typography>{x.question}</Typography>
                  </Grid>
                </Grid>
                {x.questionType === "boolean" ? (
                  <>
                    <Grid
                      sx={{ display: "flex", justifyContent: "center" }}
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid
                        className={classes.thirdGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "start",
                          alignItems: "center",
                        }}
                      >
                        <FormControl>
                          <RadioGroup
                            aria-labelledby="demo-controlled-radio-buttons-group"
                            name="level"
                            // value={value}
                            // onClick={() => props.inputFieldClicked()}
                            onChange={(e) =>
                              handleChange(
                                e,

                                x.id
                              )
                            }
                          >
                            {x.answerOptions.map((y) => (
                              <FormControlLabel
                                value={y}
                                control={
                                  <Radio
                                    sx={{
                                      color: "green",

                                      "&.Mui-checked": {
                                        color: "green",
                                      },
                                    }}
                                  />
                                }
                                label={y}
                              />
                            ))}
                          </RadioGroup>
                        </FormControl>
                      </Grid>
                    </Grid>
                  </>
                ) : x.questionType === "singleInput" ? (
                  <>
                    <Grid
                      container
                      className={classes.thirdGifCard}
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <TextField
                        size="small"
                        label=""
                        placeholder={x.placeHolder}
                        id="fullWidth"
                        onChange={(e) =>
                          handleChange(
                            e,

                            x.id
                          )
                        }
                        className={classes.about_mortgage_box}
                      />
                    </Grid>
                  </>
                ) : x.questionType === "phone" ? (
                  <>
                    <Grid
                      className={classes.thirdGifCard}
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <MuiPhoneNumber
                        size="small"
                        variant="outlined"
                        defaultCountry={"gb"}
                        onChange={(value) => handlePhoneChange(value, "phone")}
                        sx={{
                          width: "80%",
                          backgroundColor: "white",
                          borderRadius: "5px",
                        }}
                      />
                    </Grid>
                  </>
                ) : null}
              </>
            ))}

            <Grid lg={6} className={classes.homepage_button_Grid}>
              <Button
                variant="outlined"
                className={classes.homepage_button}
                onClick={PostDataHandler}
              >
                Let's Begin...
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </>
  );
};

export default Form;
