import { Box, Container, Grid } from "@mui/material";
import { useState } from "react";
import About from "../../components/about/about";
import Certificate from "../../components/certificate/certificate";
import Header from "../../components/header/header";
import Income from "../../components/income/income";
import Mortgage from "../../components/mortgage/mortgage";
import Outgoings from "../../components/outgoings/outgoing";
import Property from "../../components/property/property";
import Sidebar from "../../components/sidebar/sidebar";
import Form from "../form/form";
import classes from "./homePage.module.css";
export function NonIvestment() {
  return (
    <>
      <Grid item xs={12}>
        <Header />
      </Grid>
      <Grid container>
        <Grid item xl={2.1} lg={2.4} md={2} xs={0}>
          <Box sx={{ position: "fixed", display: { sm: "none", md: "block" } }}>
            <Sidebar />
          </Box>
        </Grid>
        <Grid item xl={6.9} lg={6.6} md={7} sm={12} xs={12}>
          <Grid item container xs={12} spacing={1} id="about">
            <About />
          </Grid>
          <Grid item container xs={12} spacing={1} id="income">
            <Income />
          </Grid>
          <Grid item container xs={12} spacing={1} id="outgoings">
            <Outgoings />
          </Grid>
          <Grid item container xs={12} spacing={1} id="property">
            <Property />
          </Grid>
          {/* </Box> */}
        </Grid>
        <Grid item xl={2} lg={2} md={2} xs={0}>
          <Box className={classes.pagelayout}>
            <Box
              sx={{ position: "fixed", display: { sm: "none", md: "block" } }}
            >
              <Certificate />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </>
  );
}
