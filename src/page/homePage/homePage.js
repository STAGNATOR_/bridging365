import React, {useEffect, useRef} from "react"
import { Co2Sharp, CollectionsOutlined } from "@mui/icons-material";
import { Box, Container, Grid } from "@mui/material";
import { useLocation } from "react-router-dom";
import { useState } from "react";
import About from "../../components/about/about";
import Certificate from "../../components/certificate/certificate";
import Header from "../../components/header/header";
import Income from "../../components/income/income";
import Mortgage from "../../components/mortgage/mortgage";
import Outgoings from "../../components/outgoings/outgoing";
import Property from "../../components/property/property";
import Sidebar from "../../components/sidebar/sidebar";
import Form from "../form/form";
import axiosClient from "../../helper";
import { send } from "emailjs-com";


import classes from "./homePage.module.css";

const HomePage = (props) => {
  const [questions, setQuestions] = useState([
    {
      id: 0,
      question: "What is the name of your company?",
      placeHolder:"Insert company name here...",
      answer: "",
      type: "mortgage",
      questionType: "singleInputField",
    },
    {
      id: 1,
      question: "What is the company registration number?",
      placeHolder:"Insert here e.g.02938464...",
      answer: "",
      type: "mortgage",
      questionType: "singleInputField",
    },
    {
      id: 2,
      question: "What is the registered company address? ",
      placeHolder:"Insert here...",
      answer: "",
      type: "mortgage",
      questionType: "singleInputField",
    },
    {
      id: 3,
      question: "Address Line 2 (optional)",
      placeHolder:"Insert here...",
      answer: "",
      type: "mortgage",
      questionType: "singleInputField",
    },
    {
      id: 4,
      question: "City or Town",
      placeHolder:"Insert here...",
      answer: "",
      type: "mortgage",
      questionType: "singleInputField",
    },
    {
      id: 5,
      question: "Postcode?",
      placeHolder:"Insert here e.g. NW14 6AX",
      answer: "",
      type: "mortgage",
      questionType: "singleInputField",
    },
    {
      id: 6,
      question: "What's your first name?",
      placeHolder:"Insert first name here...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 7,
      question: "And your surname?",
      placeHolder:"Enter your last name here...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 8,
      question: "What is your preferred Title?",
      placeHolder:"e.g. Mr, Miss, etc...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 9,
      question: "When were you born?",
      answer: new Date(),
      type: "director",
      questionType: "calendar",
    },
    {
      id: 10,
      question: "What is your home address?",
      placeHolder:"Insert here...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 11,
      question: "Address line 2 (Optional)",
      placeHolder:"Insert here...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 12,
      question: "City or Town",
      placeHolder:"Insert here...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 13,
      question: "Your home postcode?",
      placeHolder:"Insert here...",
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 14,
      question: "What's the best number to contact you on?",
      answer: "",
      type: "director",
      questionType: "phone",
    },
    {
      id: 15,
      question:
        "Your preferred e-mail address?",
      placeHolder:"Enter email address",      
      answer: "",
      type: "director",
      questionType: "singleInputField",
    },
    {
      id: 16,
      question: "Have you had any CCJ's or defaults in last 6 years?",
      answerOptions: ["yes", "no"],
      answer: "",
      type: "director",
      questionType: "boolean",
    },
    {
      id: 17,
      question: "Have you ever been declared bankrupt?",
      answerOptions: ["yes", "no"],
      answer: "",
      type: "director",
      questionType: "boolean",
    },
   
    {
      id: 18,
      question: "What is the security address?",
      placeHolder:"1st line of the property address?",
      answer: "",
      type: "security",
      questionType: "singleInputField",
    },
    {
      id: 110,
      question: "Address line 2 (optional)",
      placeHolder:"Optional",
      answer: "",
      type: "security",
      questionType: "singleInputField",
    },
    {
      id: 120,
      question: "Which city or town is the property located in?",
      placeHolder:"City or town name...",
      answer: "",
      type: "security",
      questionType: "singleInputField",
    },
    {
      id: 130,
      question: "What is the postcode?",
      placeHolder:"Insert here...",
      answer: "",
      type: "security",
      questionType: "singleInputField",
    },
    {
      id:19,
      question: "What is the estimated property valuation?",
      placeHolder:"£...",
      answer: "",
      type: "security",
      questionType: "singleInputField",
    },
    {
      id: 20,
      question: "Please select the best description of the property below",
      answer: "",
      type: "security",
      questionType: "boolean",
      answerOptions: ["House","Flat","HMO (House in Multiple Occupation)","Commercial property","Land with or without planning permission","Other"],
    },
    {
      id: 21,
      question: "How many bedrooms does the property have?",
      placeHolder:"Total number of bedrooms...",
      answer: "",
      type: "house",
      questionType: "singleInputField",
    },
    {
      id: 22,
      question: "How many floors are in the building?",
      placeHolder:"Total number of floors...",
      answer: "",
      type: "house",
      questionType: "singleInputField",
    },
    {
      id: 23,
      question: "Is it an ex-council property?",
      answer: "",
      type: "house",
      answerOptions: ["Yes", "No"],
      questionType: "boolean",
    },
    {
      id: 24,
      question:
        "Is property of standard construction? Please select below and tell us more.",
      placeHolder:"select and tell us more",
      answer: "",
      type: "house",
      questionType: "singleInputField",
    },
    {
      id: 25,
      question: "How many bedrooms does the property have?",
      placeHolder:"Total number of bedrooms...",
      answer: "",
      type: "flat",
      questionType: "singleInputField",
    },
    {
      id: 26,
      question: "Which floor of the building is the flat on?",
      placeHolder:"Floor number...",
      answer: "",
      type: "flat",
      questionType: "singleInputField",
    },
    {
      id: 27,
      question: "How many floors does the building have? ",
      placeHolder:"number of floor",
      answer: "",
      type: "flat",
      questionType: "singleInputField",
    },
    {
      id: 28,
      question: "Is the property Ex-Council? ",
      answer: "",
      type: "flat",
      answerOptions: ["Yes", "No"],

      questionType: "boolean",
    },
    {
      id: 29,
      question:
        "Does the building have any retail units?",
      answer: "",
      questionType: "boolean",
      answerOptions: ["Yes", "No"],

      type: "flat",
    },
    {
      id: 30,
      question: "What is the licence status of HMO?",
      answer: "",
      type: "hmo",
      questionType: "dropdown",
      label: "Licence Status",
    },
    {
      id: 31,
      question: "How many lettable rooms does the HMO have?",
      answer: "",
      type: "hmo",
      questionType: "dropdown",
      label: "HMO Rooms",
    },
    {
      id: 32,
      question: "Does it have any communal rooms?",
      answer: "",
      type: "hmo",
      answerOptions: ["Yes", "No"],
      questionType: "boolean",
    },
    {
      id: 33,
      question: "How many communal rooms are there? please describe in the box",
      placeHolder:"Total number of communal rooms...",
      answer: "",
      type: "hmo",
      questionType: "singleInputField",
    },
    {
      id: 34,
      question: "Do you have individual AST's for your tenants?",
      answer: "",
      type: "hmo",
      answerOptions: ["Yes", "No"],
      questionType: "boolean",
    },
    {
      id: 35,
      question:
        "Do you run or plan to run your own business from the premises?",
      answer: "",
      type: "commercial",
      answerOptions: ["Yes", "No"],
      questionType: "boolean",
    },
    {
      id: 36,
      question: "How would you describe the premises?",
      placeHolder:"Describe as...",
      answer: "",
      type: "commercial",
      questionType: "dropdown",
    },
    {
      id: 37,
      question: "Is there any planning permission in place?",
      answer: "",
      type: "land",
      subType: "Land",
      planningNumber: "",
      detail: "",
      answerOptions: ["Yes", "No"],
    },
    {
      id: 38,
      question: "What is purpose of land?",
      answer: "",
      type: "land1",
      planningNumber: "",
      detail: "",
      subType: "subland",

      answerOptions: ["Yes", "No"],
    },
    {
      id: 39,
      question: " Please describe the asset:",
      placeHolder:"describe the asset",
      answer: "",
      type: "other",
    },
    {
      id: 40,
      question: "Do you know the tenure of the property?",
      answer: "",
      type: "security",
      questionType: "boolean",
      answerOptions: ["Freehold", "Leasehold", "Other"],
      SpecifyCondition: "",
      LeaseYears: "",
    },
    {
      id: 41,
      question: "Would you like to add another property?",
      answer: "",
      type: "security",
      questionType: "boolean",
      answerOptions: ["Yes", "No"],
    },
    {
      id: 42,
      question: "What is the purpose of the bridging loan?",
      answer: "",
      type: "loan",
      questionType: "dropdown",
      label: "Age",
    },
    {
      id: 43,
      question: "Is your current loan from a bridging lender?",
      answer: "",
      type: "loan1",
      questionType: "boolean",
      answerOptions: ["Yes", "No"],
    },
    {
      id: 44,
      question: "When do you need the loan to be completed by?",
      answer: "",
      type: "loan3",
      questionType: "boolean",
      answerOptions: ["Yes", "No"],
    },
    {
      id: 45,
      question: "How much would you like to borrow?",
      placeHolder:"£...",
      answer: "",
      type: "loan2",
      questionType: "singleInput",
    },
    {
      id: 46,
      question: "Please select the number of months you would like the loan for",
      answer: "",
      type: "loan2",
      questionType: "dropdown",
      label: "month",
    },
    {
      id: 47,
      question:
        "Would you like to pay the interest at the end of every month, or at the end of the loan?",
      answer: "",
      type: "loan2",
      questionType: "boolean",
      answerOptions: ["I'd like to pay the interest due every month", "I'd like to make one payment at the end of the loan"],
    },
    {
      id: 48,
      question: "How will you be paying the loan back?",
      answer: "",
      type: "loan2",
      questionType: "boolean",
      answerOptions: [
        "Selling the property",
        "Refinancing to a term lender",
        "Other-please specify",
      ],
    },
    {
      id: 49,
      question: "Please tell us anything else that is relevant to the application?",
      placeHolder:"Let us know here...",
      answer: "",
      type: "property",
    },
  ]);
  const [companyArray, setCompanyArray] = useState([]);

  let INITIAL_OBJECT = {
    question: "",
    answer: "",
  };

  const myDiv = useRef(null)

  const {state:iddd} = useLocation()
  const {divId} = iddd

  useEffect(()=>{
    console.log("myDivmyDiv",myDiv)
    if(divId){
      myDiv?.current?.scrollIntoView({ behavior: 'smooth' });
    }
  },[myDiv])



  const [companyQuestion, setComapnyQuestion] = useState({});

  const [value, setValue] = useState("");

  const [state, setState] = useState({});
function inputFieldClicked() {}
function handleOutingRadioChange(e, id) {
  const { value } = e.target;
  console.log("id", id);
  console.log("e.target", e.target);
  console.log("value", value);
  setDropValue(value);

  const previousArray = [...questions];

  //Find index of specific object using findIndex method.
  let objIndex = previousArray.findIndex((obj) => obj.id == id);

  //Log object to Console.
  console.log("Before update: ", previousArray[objIndex]);

  //Update object's name property.
  previousArray[objIndex].answer = value;

  //Log object to console again.
  console.log("After update: ", previousArray[objIndex]);
  setQuestions(previousArray);
}
const [dropValue, setDropValue] = useState("");
function handleChange(e, id) {
  const { value } = e.target;
  console.log("id", id);
  console.log("e.target", e.target);
  console.log("value", value);

  const previousArray = [...questions];

  //Find index of specific object using findIndex method.
  let objIndex = previousArray.findIndex((obj) => obj.id == id);

  //Log object to Console.
  console.log("Before update: ", previousArray[objIndex]);

  //Update object's name property.
  previousArray[objIndex].answer = value;

  //Log object to console again.
  console.log("After update: ", previousArray[objIndex]);
  setQuestions(previousArray);
}
function handlePlanningChange(e, id) {
  const { value } = e.target;
  const previousArray = [...questions];
  let objIndex = previousArray.findIndex((obj) => obj.id == id);
  //Update object's name property.
  previousArray[objIndex].planningNumber = value;
  // previousArray[objIndex].detail = value;

  setQuestions(previousArray);
}
function handleLeaseChange(e, id) {
  const { value } = e.target;
  const previousArray = [...questions];
  let objIndex = previousArray.findIndex((obj) => obj.id == id);

  previousArray[objIndex].LeaseYears = value;
  setQuestions(previousArray);
}
function handleSpecifyChange(e, id) {
  const { value } = e.target;
  const previousArray = [...questions];
  let objIndex = previousArray.findIndex((obj) => obj.id == id);

  previousArray[objIndex].SpecifyCondition = value;
  setQuestions(previousArray);
}
function handleDetailChange(e, id) {
  const { value } = e.target;
  console.log("id", id);
  console.log("e.target", e.target);
  console.log("value", value);

  const previousArray = [...questions];

  //Find index of specific object using findIndex method.
  let objIndex = previousArray.findIndex((obj) => obj.id == id);

  //Log object to Console.
  console.log("Before update: ", previousArray[objIndex]);

  //Update object's name property.
  previousArray[objIndex].detail = value;
  //Log object to console again.
  console.log("After update: ", previousArray[objIndex]);
  setQuestions(previousArray);
}

function handlePhoneChange(value, type) {
  console.log("Value", value);
  const previousArray = [...questions];
  let objIndex = previousArray.findIndex((obj) => obj.questionType === type);
  previousArray[objIndex].answer = value;
  setQuestions(previousArray);
}
const handleSubmit = () => {
  console.log("final submission", questions);
  axiosClient()
    .put(`mortage/updateMortage/${props.id}`, { questions })
    send(
      "service_714m9nk",
      "template_fmhrq0e",
      {sender:"ali_arsam",sender:"aliarsam013@gmail.com",message:"hello"},
      "i_xd3oPZwXG8ANmap",
    )
    .then((res) => { 
      // props.setId(res.data.data._id);
      console.log("api      ", res);

    });
};
return (
  <>
    <Grid item xs={12}>
      <Header />
    </Grid>
    <Grid container>
      <Grid item xl={2.1} lg={2.4} md={2} xs={0}>
        <Box sx={{ position: "fixed", display: { sm: "none", md: "block" } }}>
          <Sidebar questions={questions} handleSubmit={handleSubmit} />
        </Box>
      </Grid>
      <Grid item xl={6.9} lg={6.6} md={7} sm={12} xs={12}>
        <Grid item container xs={12} spacing={1} id="outgoings">
          <Outgoings
            handleChange={handleChange}
            handleOutingRadioChange={handleOutingRadioChange}
            inputFieldClicked={inputFieldClicked}
            state={state}
            questions={questions}
            dropValue={dropValue}
          />
        </Grid>
        <Grid item container xs={12} spacing={1} id="income">
          <Income
            handleChange={handleChange}
            inputFieldClicked={inputFieldClicked}
            state={state}
            questions={questions}
            handlePlanningChange={handlePlanningChange}
            handleDetailChange={handleDetailChange}
            handleLeaseChange={handleLeaseChange}
            handleSpecifyChange={handleSpecifyChange}
          />
        </Grid>
        <Grid ref={myDiv} item container xs={12} spacing={1} id="mortgage">
          <Mortgage
            handleChange={handleChange}
            inputFieldClicked={inputFieldClicked}
            state={state}
            questions={questions}
            handlePhoneChange={handlePhoneChange}
          />
        </Grid>
        <Grid item container xs={12} spacing={1} id="about">
          <About
            handleChange={handleChange}
            inputFieldClicked={inputFieldClicked}
            state={state}
            questions={questions}
            handlePhoneChange={handlePhoneChange}
          />
        </Grid>

        <Grid item container xs={12} spacing={1} id="property">
          <Property
            handleChange={handleChange}
            inputFieldClicked={inputFieldClicked}
            state={state}
            questions={questions}
          />
        </Grid>
        {/* </Box> */}
      </Grid>
      <Grid item xl={2} lg={2} md={2} xs={0}>
        <Box className={classes.pagelayout}>
          <Box sx={{ position: "fixed", display: { sm: "none", md: "block" } }}>
            <Certificate />
          </Box>
        </Box>
      </Grid>
    </Grid>
  </>
);
};

export default HomePage;
