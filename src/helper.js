import axios from "axios";

let BASE_URL = "https://mortgage-be.herokuapp.com/";

// if (process.env.NODE_ENV === "development") {
//   // BASE_URL = process.env.REACT_APP_BASE_URL_DEV;
//   // BASE_URL = 'http://192.168.0.64:5000';
//   // BASE_URL = 'http://caba-182-185-150-46.ngrok.io';
//   // BASE_URL = process.env.REACT_APP_BASE_URL_PROD;
// }

// if (process.env.NODE_ENV === "production") {
//   BASE_URL = process.env.REACT_APP_BASE_URL_PROD;
// }

export default function axiosClient() {
  let defaultOptions = {
    baseURL: BASE_URL,
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      accept: "application/json",
    },
  };

  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(function (config) {
    config.headers.common = {
      "x-auth-token": `${localStorage.getItem("token")}`,
    };
    return config;
  });

  return instance;
}
