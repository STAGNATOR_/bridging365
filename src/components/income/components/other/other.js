import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import OutlinedInput from "@mui/material/OutlinedInput";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import infoimg from "../../../../Assets/info.png";
import classes from "../../../income/income.module.css";
import { useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import Checkbox from "@mui/material/Checkbox";

const Other = (props) => {
  // const [obj, setObj] = useState("");

  // const handleChange = (event) => {
  //   setObj(event.target.value);
  // };
  return (
    <>
      {props.questions
        .filter((item) => item.type === "other")
        .map((z) => (
          <>
            <Grid
              sx={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2rem",
                marginTop: "2rem",
              }}
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <Grid
                className={classes.secGifCard}
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <Typography>{z.question}</Typography>
                <img
                  src={infoimg}
                  alt="info"
                  style={{ marginLeft: "2rem" }}
                  width="20px"
                />
              </Grid>
            </Grid>
            <Grid
              container
              className={classes.thirdGifCard}
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <TextField
                size="small"
                label=""
                id="fullWidth"
                placeholder={z.placeHolder}
                sx={{ marginTop: "1rem", marginBottom: "1rem" }}
                className={classes.about_mortgage_box}
                onChange={(e) =>
                  props.handleChange(
                    e,

                    z.id
                  )
                }
              />
            </Grid>
          </>
        ))}
      {/* 2 */}
      {/* <Grid
          sx={{
            display: "flex",
            justifyContent: "center",
            marginBottom: "2rem",
            marginTop: "2rem",
          }}
          lg={12}
          md={12}
          sm={12}
          xs={12}
        >
          <Grid
            className={classes.secGifCard}
            sx={{
              display: "flex",
              justifyContent: "start",
              alignItems: "center",
            }}
          >
            <Typography>What is purpose of Land?</Typography>
            <img
              src={infoimg}
              alt="info"
              style={{ marginLeft: "2rem" }}
              width="20px"
            />
          </Grid>
        </Grid>
        <Grid
          container
          className={classes.thirdGifCard}
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <FormControl className={classes.about_mortgage_box}>
            <InputLabel id="demo-simple-select-label">Purpose</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={obj}
              label="Purpose"
              onChange={handleChange}
            >
              <MenuItem value={1}>Resedential</MenuItem>
              <MenuItem value={2}>Commercial</MenuItem>
              <MenuItem value={3}>Agricultural</MenuItem>
              <MenuItem value={4}>Recreational</MenuItem>
              <MenuItem value={5}>Transport</MenuItem>
            </Select>
          </FormControl>
        </Grid> */}
    </>
  );
};

export default Other;
