import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";

import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import infoimg from "../../../../Assets/info.png";
import classes from "../../../income/income.module.css";
import { useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";

const HMO = (props) => {
  const [age, setAge] = useState("");
  const [rooms, setRooms] = useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  const handleRoomsChange = (event) => {
    setRooms(event.target.value);
  };
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  return (
    <>
      {props.questions
        .filter((item) => item.type === "hmo")
        .map((z) => (
          <>
            <Grid
              sx={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2rem",
                marginTop: "2rem",
              }}
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <Grid
                className={classes.secGifCard}
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <Typography>{z.question}</Typography>
                <img
                  src={infoimg}
                  alt="info"
                  style={{ marginLeft: "2rem" }}
                  width="20px"
                />
              </Grid>
            </Grid>
            {z.questionType === "dropdown" ? (
              <>
                <Grid
                  container
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <FormControl className={classes.about_mortgage_box}>
                    {/* <InputLabel id="demo-simple-select-label">
                      {z.label}
                    </InputLabel> */}
                    <Select
                      input={<OutlinedInput />}
                      renderValue={(selected) => {
                        if (selected.length === 0) {
                          return <em>Placeholder</em>;
                        }

                        return selected.join(", ");
                      }}
                      MenuProps={MenuProps}
                      inputProps={{ "aria-label": "Without label" }}
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          z.id
                        )
                      }
                    >
                      <MenuItem value={"Currently Held"}>
                        Currently Held
                      </MenuItem>
                      <MenuItem value={"Application in progress"}>
                        Application in progress
                      </MenuItem>
                      <MenuItem value={"Declined requiring alteration"}>
                        Declined requiring alteration
                      </MenuItem>
                      <MenuItem value={"About to submit"}>
                        About to submit
                      </MenuItem>
                      <MenuItem value={"Not required"}>Not required</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </>
            ) : z.questionType === "boolean" ? (
              <>
                <Grid
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "start",
                    alignItems: "center",
                  }}
                >
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-controlled-radio-buttons-group"
                      name="controlled-radio-buttons-group"
                      value={props.value}
                      // onClick={() => props.inputFieldClicked()}
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          z.id
                        )
                      }
                    >
                      {z.answerOptions.map((n) => (
                        <FormControlLabel
                          value={n}
                          control={
                            <Radio
                              sx={{
                                color: "#ebbe34",

                                "&.Mui-checked": {
                                  color: "#ebbe34",
                                },
                              }}
                            />
                          }
                          label={n}
                        />
                      ))}
                    </RadioGroup>
                  </FormControl>
                </Grid>
              </>
            ) : z.questionType === "singleInputField" ? (
              <>
                <Grid
                  container
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <TextField
                    onChange={(e) =>
                      props.handleChange(
                        e,

                        z.id
                      )
                    }
                    size="small"
                    label=""
                    id="fullWidth"
                    className={classes.about_mortgage_box}
                  />
                </Grid>
              </>
            ) : null}
          </>
        ))}

      {/* 
      <Grid
        sx={{
          display: "flex",
          justifyContent: "center",
          marginBottom: "2rem",
          marginTop: "2rem",
        }}
        lg={12}
        md={12}
        sm={12}
        xs={12}
      >
        <Grid
          className={classes.secGifCard}
          sx={{
            display: "flex",
            justifyContent: "start",
            alignItems: "center",
          }}
        >
          <Typography>How many rooms does the HMO let?</Typography>
          <img
            src={infoimg}
            alt="info"
            style={{ marginLeft: "2rem" }}
            width="20px"
          />
        </Grid>
      </Grid>
      <Grid
        container
        className={classes.thirdGifCard}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <FormControl className={classes.about_mortgage_box}>
          <InputLabel id="demo-simple-select-label">rooms</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={age}
            label="rooms"
            onChange={handleChange}
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
            <MenuItem value={4}>4</MenuItem>
            <MenuItem value={5}>5</MenuItem>
            <MenuItem value={6}>6</MenuItem>
            <MenuItem value={7}>7</MenuItem>
            <MenuItem value={8}>8</MenuItem>
            <MenuItem value={9}>9</MenuItem>
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={11}>above</MenuItem>
          </Select>
        </FormControl>
      </Grid>
     

      <Grid
        sx={{
          display: "flex",
          justifyContent: "center",
          marginBottom: "2rem",
          marginTop: "2rem",
        }}
        lg={12}
        md={12}
        sm={12}
        xs={12}
      >
        <Grid
          className={classes.secGifCard}
          sx={{
            display: "flex",
            justifyContent: "start",
            alignItems: "center",
          }}
        >
          <Typography>Does it have any communal rooms?</Typography>
          <img
            src={infoimg}
            alt="info"
            style={{ marginLeft: "2rem" }}
            width="20px"
          />
        </Grid>
      </Grid>
      <Grid
        container
        className={classes.thirdGifCard}
        sx={{
          display: "flex",
          justifyContent: "start",
          alignItems: "center",
        }}
      >
        <FormControl>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            value={props.value}
            onChange={props.handleChange}
          >
            <FormControlLabel
              value="byProperty"
              control={
                <Radio
                  sx={{
                    color: "green",

                    "&.Mui-checked": {
                      color: "green",
                    },
                  }}
                />
              }
              label="Yes"
            />
            <FormControlLabel
              value="remortgage"
              control={
                <Radio
                  sx={{
                    color: "green",

                    "&.Mui-checked": {
                      color: "green",
                    },
                  }}
                />
              }
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid
        sx={{
          display: "flex",
          justifyContent: "center",
          marginBottom: "2rem",
          marginTop: "2rem",
        }}
        lg={12}
        md={12}
        sm={12}
        xs={12}
      >
        <Grid
          className={classes.secGifCard}
          sx={{
            display: "flex",
            justifyContent: "start",
            alignItems: "center",
          }}
        >
          <Typography>
            How many communal rooms are there? please describe in the box
          </Typography>
          <img
            src={infoimg}
            alt="info"
            style={{ marginLeft: "2rem" }}
            width="20px"
          />
        </Grid>
      </Grid>
      <Grid
        container
        className={classes.thirdGifCard}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <TextField
          size="small"
          label=""
          id="fullWidth"
          className={classes.about_mortgage_box}
        />
      </Grid>
     
      <Grid
        sx={{
          display: "flex",
          justifyContent: "center",
          marginBottom: "2rem",
          marginTop: "2rem",
        }}
        lg={12}
        md={12}
        sm={12}
        xs={12}
      >
        <Grid
          className={classes.secGifCard}
          sx={{
            display: "flex",
            justifyContent: "start",
            alignItems: "center",
          }}
        >
          <Typography>Do you have AST's for your Tenants?</Typography>
          <img
            src={infoimg}
            alt="info"
            style={{ marginLeft: "2rem" }}
            width="20px"
          />
        </Grid>
      </Grid>
      <Grid
        container
        className={classes.thirdGifCard}
        sx={{
          display: "flex",
          justifyContent: "start",
          alignItems: "center",
        }}
      >
        <FormControl>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            value={props.value}
            onChange={props.handleChange}
          >
            <FormControlLabel
              value="byProperty"
              control={
                <Radio
                  sx={{
                    color: "green",

                    "&.Mui-checked": {
                      color: "green",
                    },
                  }}
                />
              }
              label="Yes"
            />
            <FormControlLabel
              value="remortgage"
              control={
                <Radio
                  sx={{
                    color: "green",

                    "&.Mui-checked": {
                      color: "green",
                    },
                  }}
                />
              }
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid> */}
    </>
  );
};

export default HMO;
