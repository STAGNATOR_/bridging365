import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import OutlinedInput from "@mui/material/OutlinedInput";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import infoimg from "../../../../Assets/info.png";
import classes from "../../../income/income.module.css";
import { useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import Checkbox from "@mui/material/Checkbox";

const Commercials = (props) => {
  const [desc, setDesc] = useState("");

  const handleChange = (event) => {
    setDesc(event.target.value);
  };
  return (
    <>
      {props.questions
        .filter((item) => item.type === "commercial")
        .map((z) => (
          <>
            <Grid
              sx={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2rem",
                marginTop: "2rem",
              }}
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <Grid
                className={classes.secGifCard}
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <Typography>{z.question}</Typography>
                <img
                  src={infoimg}
                  alt="info"
                  style={{ marginLeft: "2rem" }}
                  width="20px"
                />
              </Grid>
            </Grid>
            {z.questionType === "dropdown" ? (
              <>
                <Grid
                  container
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <FormControl className={classes.about_mortgage_box}>
                    <InputLabel id="demo-simple-select-label">
                      {z.label}
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      // value={age}
                      label="licence status"
                      // onChange={handleChange}
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          z.id
                        )
                      }
                    >
                      <MenuItem value={"Currently Held"}>
                        Currently Held
                      </MenuItem>
                      <MenuItem value={"Application in progress"}>
                        Application in progress
                      </MenuItem>
                      <MenuItem value={"Declined requiring alteration"}>
                        Declined requiring alteration
                      </MenuItem>
                      <MenuItem value={"About to submit"}>
                        About to submit
                      </MenuItem>
                      <MenuItem value={"Not required"}>Not required</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </>
            ) : z.questionType === "boolean" ? (
              <>
                <Grid
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "start",
                    alignItems: "center",
                  }}
                >
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-controlled-radio-buttons-group"
                      name="controlled-radio-buttons-group"
                      value={props.value}
                      // onClick={() => props.inputFieldClicked()}
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          z.id
                        )
                      }
                    >
                      {z.answerOptions.map((n) => (
                        <FormControlLabel
                          value={n}
                          control={
                            <Radio
                              sx={{
                                color: "#ebbe34",

                                "&.Mui-checked": {
                                  color: "#ebbe34",
                                },
                              }}
                            />
                          }
                          label={n}
                        />
                      ))}
                    </RadioGroup>
                  </FormControl>
                </Grid>
              </>
            ) : null}{" "}
          </>
        ))}
      {/* 2 */}
      {/* <Grid
        sx={{
          display: "flex",
          justifyContent: "center",
          marginBottom: "2rem",
          marginTop: "2rem",
        }}
        lg={12}
        md={12}
        sm={12}
        xs={12}
      >
        <Grid
          className={classes.secGifCard}
          sx={{
            display: "flex",
            justifyContent: "start",
            alignItems: "center",
          }}
        >
          <Typography>How would you describe the premises?</Typography>
          <img
            src={infoimg}
            alt="info"
            style={{ marginLeft: "2rem" }}
            width="20px"
          />
        </Grid>
      </Grid>
      <Grid
        container
        className={classes.thirdGifCard}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <FormControl className={classes.about_mortgage_box}>
          <InputLabel id="demo-simple-select-label">description</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={desc}
            label="description"
            onChange={handleChange}
          >
            <MenuItem value={1}>
              <Checkbox />
              Offices
            </MenuItem>
            <MenuItem value={2}>
              <Checkbox />
              Retail-retail stores,shopping centers,shops
            </MenuItem>
            <MenuItem value={3}>
              <Checkbox />
              Industrial-warehouses,factories
            </MenuItem>
            <MenuItem value={4}>
              <Checkbox />
              Leisure-hotels,pubs,returants,cafes,sports facilities
            </MenuItem>
            <MenuItem value={5}>
              <Checkbox />
              Health care-medical centers,hospitals,nursing homes
            </MenuItem>
          </Select>
        </FormControl>
      </Grid> */}
    </>
  );
};

export default Commercials;
