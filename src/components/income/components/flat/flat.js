import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import infoimg from "../../../../Assets/info.png";
import classes from "../../../income/income.module.css";

const Flat = (props) => {
  return (
    <>
      {props.questions
        .filter((item) => item.type === "flat")
        .map((z) => (
          <>
            <Grid
              sx={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2rem",
                marginTop: "2rem",
              }}
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <Grid
                className={classes.secGifCard}
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <Typography>{z.question}</Typography>
                <img
                  src={infoimg}
                  alt="info"
                  style={{ marginLeft: "2rem" }}
                  width="20px"
                />
              </Grid>
            </Grid>

            {z.questionType === "singleInputField" ? (
              <>
                <Grid
                  container
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <TextField
                    onChange={(e) =>
                      props.handleChange(
                        e,

                        z.id
                      )
                    }
                    size="small"
                    label=""
                    placeholder={z.placeHolder}
                    id="fullWidth"
                    className={classes.about_mortgage_box}
                  />
                </Grid>
              </>
            ) : z.questionType === "boolean" ? (
              <>
                <Grid
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "start",
                    alignItems: "center",
                  }}
                >
                  <FormControl>
                    <RadioGroup
                      aria-labelledby="demo-controlled-radio-buttons-group"
                      name="controlled-radio-buttons-group"
                      value={props.value}
                      // onClick={() => props.inputFieldClicked()}
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          z.id
                        )
                      }
                    >
                      {z.answerOptions.map((n) => (
                        <FormControlLabel
                          value={n}
                          control={
                            <Radio
                              sx={{
                                color: "#ebbe34",

                                "&.Mui-checked": {
                                  color: "#ebbe34",
                                },
                              }}
                            />
                          }
                          label={n}
                        />
                      ))}
                    </RadioGroup>
                  </FormControl>
                </Grid>
              </>
            ) : null}
          </>
        ))}
    </>
  );
};

export default Flat;
