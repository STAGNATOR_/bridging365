import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import OutlinedInput from "@mui/material/OutlinedInput";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import infoimg from "../../../../Assets/info.png";
import classes from "../../../income/income.module.css";
import { useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import Checkbox from "@mui/material/Checkbox";

const Land = (props) => {
  const [obj, setObj] = useState("");

  // const handleChange = (event) => {
  //   setObj(event.target.value);
  // };
  const consoleHandler = (e) => {
    setObj(e.target.value);
    console.log("Radio Clicked: ", e.target.value);
  };

  return (
    <>
      {props.questions
        .filter((item) => item.type === "land")
        .map((z) => (
          <>
            <Grid
              sx={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2rem",
                marginTop: "2rem",
              }}
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <Grid
                className={classes.secGifCard}
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <Typography>{z.question}</Typography>
                <img
                  src={infoimg}
                  alt="info"
                  style={{ marginLeft: "2rem" }}
                  width="20px"
                />
              </Grid>
            </Grid>
            <Grid
              container
              className={classes.thirdGifCard}
              sx={{
                display: "flex",
                justifyContent: "start",
                alignItems: "center",
              }}
            >
              <Grid container lg={12} md={12} sm={12} xs={12}>
                <FormControl>
                  <RadioGroup
                    aria-labelledby="demo-controlled-radio-buttons-group"
                    name="controlled-radio-buttons-group"
                    value={props.value}
                    onChange={(e) =>
                      props.handleChange(
                        e,

                        z.id
                      )
                    }
                  >
                    {z.answerOptions.map((b) => (
                      <FormControlLabel
                        value={b}
                        control={
                          <Radio
                            onClick={(e) => consoleHandler(e)}
                            sx={{
                              color: "#ebbe34",

                              "&.Mui-checked": {
                                color: "#ebbe34",
                              },
                            }}
                          />
                        }
                        label={b}
                      />
                    ))}
                  </RadioGroup>
                </FormControl>
              </Grid>
              {z.answer === "Yes" ? (
                <>
                  <TextField
                    size="small"
                    label="Planning Number"
                    id="fullWidth"
                    sx={{ marginTop: "1rem", marginBottom: "1rem" }}
                    className={classes.about_mortgage_box}
                    onChange={(e) => props.handlePlanningChange(e, z.id)}
                  />
                  <TextField
                    size="small"
                    label="Detail Box"
                    id="fullWidth"
                    sx={{ marginTop: "1rem", marginBottom: "1rem" }}
                    className={classes.about_mortgage_box}
                    onChange={(e) => props.handleDetailChange(e, z.id)}
                  />
                </>
              ) : null}
            </Grid>
          </>
        ))}
      {obj === "No"
        ? props.questions
            .filter((item) => item.type === "land1")
            .map((a) => (
              <>
                <Grid
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    marginBottom: "2rem",
                    marginTop: "2rem",
                  }}
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                >
                  <Grid
                    className={classes.secGifCard}
                    sx={{
                      display: "flex",
                      justifyContent: "start",
                      alignItems: "center",
                    }}
                  >
                    <Typography>{a.question}</Typography>
                    <img
                      src={infoimg}
                      alt="info"
                      style={{ marginLeft: "2rem" }}
                      width="20px"
                    />
                  </Grid>
                </Grid>

                <Grid
                  container
                  className={classes.thirdGifCard}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <FormControl className={classes.about_mortgage_box}>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      // value={props.value}
                      label=""
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          a.id
                        )
                      }
                    >
                      <MenuItem value={"Residential"}>Residential</MenuItem>
                      <MenuItem value={"Commercial"}>Commercial</MenuItem>
                      <MenuItem value={"Agricultural"}>Agricultural</MenuItem>
                      <MenuItem value={"Recreational"}>Recreational</MenuItem>
                      <MenuItem value={"Transport"}>Transport</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </>
            ))
        : null}
    </>
  );
}

export default Land;
