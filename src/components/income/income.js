import { useState } from "react";
import { Container, Grid, Typography, TextField, Box } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import infoimg from "../../Assets/info.png";
import green from "../../Assets/green.png";
import sofa from "../../Assets/sofa.gif";

import classes from "./income.module.css";
import House from "./components/House/house";
import Flat from "./components/flat/flat";
import HMO from "./components/HMO/HMO";
import Commercials from "./components/commercial/commercial";
import Land from "./components/land/land";
import Other from "./components/other/other";

const Income = (props) => {
  const [value, setValue] = useState("");
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  return (
    <>
      <Container>
        <form>
          <Grid
            container
            sx={{
              justifyContent: "center",
              marginTop: "5rem",
              marginLeft: "auto",
              marginRight: "auto",
            }}
            lg={10}
            md={7}
            sm={12}
            xs={12}
          >
            <Grid sx={{ textAlign: "center" }}>
              <Grid>
                <Typography>Section 2 / 5: Security Details?</Typography>
                <img src={green} alt="paper" />
              </Grid>

              <img src={sofa} alt="bird" className={classes.house} />
              <Grid conatiner className={classes.gifCard}>
                <Typography>
                Now we know how much you’re looking to borrow, tell us about the asset you are borrowing against?
                </Typography>
              </Grid>
              {props.questions
                .filter((item) => item.type === "security")
                .map((x) => (
                  <>
                    <Grid
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        marginBottom: "2rem",
                        marginTop: "2rem",
                      }}
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid
                        className={classes.secGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "start",
                          alignItems: "center",
                        }}
                      >
                        <Typography>{x.question}</Typography>
                        <img
                          src={infoimg}
                          alt="info"
                          style={{ marginLeft: "2rem" }}
                          width="20px"
                        />
                      </Grid>
                    </Grid>
                    {x.questionType === "singleInputField" ? (
                      <>
                        <Grid
                          container
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <TextField
                            size="small"
                            label=""
                            placeholder={x.placeHolder}
                            id="fullWidth"
                            onChange={(e) =>
                              props.handleChange(
                                e,

                                x.id
                              )
                            }
                            className={classes.about_mortgage_box}
                          />
                        </Grid>
                      </>
                    ) : x.questionType === "boolean" ? (
                      <>
                        {/* Radio Grid Start */}
                        <Grid
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "start",
                            alignItems: "center",
                          }}
                        >
                          <FormControl>
                            <RadioGroup
                              aria-labelledby="demo-controlled-radio-buttons-group"
                              name="controlled-radio-buttons-group"
                              value={props.value}
                              // onClick={() => props.inputFieldClicked()}
                              onChange={(e) =>
                                props.handleChange(
                                  e,

                                  x.id
                                )
                              }
                            >
                              {x.answerOptions.map((y) => (
                                <FormControlLabel
                                  value={y}
                                  control={
                                    <Radio
                                      sx={{
                                        color: "#ebbe34",

                                        "&.Mui-checked": {
                                          color: "#ebbe34",
                                        },
                                      }}
                                    />
                                  }
                                  label={y}
                                />
                              ))}
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                        {/* Radio Grid End */}
                      </>
                    ) : null}
                    {x.answer === "LeaseHold" ? (
                      <>
                        <Grid
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            marginTop: "2rem",
                          }}
                        >
                          <TextField
                            size="small"
                            label="Lease Years"
                            id="fullWidth"
                            sx={{ marginTop: "1rem", marginBottom: "1rem" }}
                            className={classes.about_mortgage_box}
                            onChange={(e) => props.handleLeaseChange(e, x.id)}
                          />
                        </Grid>
                      </>
                    ) : x.answer === "other" ? (
                      <>
                        <Grid
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            marginTop: "2rem",
                          }}
                        >
                          <TextField
                            size="small"
                            label=""
                            id="fullWidth"
                            sx={{ marginTop: "1rem", marginBottom: "1rem" }}
                            className={classes.about_mortgage_box}
                            onChange={(e) => props.handleSpecifyChange(e, x.id)}
                          />
                        </Grid>
                      </>
                    ) : null}
                    {x.answer === "Flat" ? (
                      <Flat
                        handleChange={props.handleChange}
                        questions={props.questions}
                      />
                    ) : x.answer === "House" ? (
                      <House
                        handleChange={props.handleChange}
                        questions={props.questions}
                      />
                    ) : x.answer === "Commercial" ? (
                      <Commercials
                        handleChange={props.handleChange}
                        questions={props.questions}
                      />
                    ) : x.answer === "HMO" ? (
                      <HMO
                        handleChange={props.handleChange}
                        questions={props.questions}
                      />
                    ) : x.answer === "Land" ? (
                      <Land
                        handleChange={props.handleChange}
                        questions={props.questions}
                        handlePlanningChange={props.handlePlanningChange}
                        handleDetailChange={props.handleDetailChange}
                      />
                    ) : x.answer === "Other" ? (
                      <Other
                        handleChange={props.handleChange}
                        questions={props.questions}
                      />
                    ) : null}
                  </>
                ))}
            </Grid>
          </Grid>
        </form>
      </Container>
    </>
  );
};

export default Income;
