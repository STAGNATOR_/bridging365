import { useState } from "react";
import { Container, Grid, Typography, TextField, Box } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import infoimg from "../../Assets/info.png";
import pad from "../../Assets/pad.png";
import house from "../../Assets/house-red.gif";
import MuiPhoneNumber from "material-ui-phone-number";

import classes from "./mortgage.module.css";
import { QuestionMarkSharp } from "@mui/icons-material";
import { type } from "@testing-library/user-event/dist/type";

const Mortgage = (props) => {
  const [value, setValue] = useState("");
  // const handleChange = (event) => {
  //   setValue(event.target.value);
  // };
  return (
    <>
      <Container>
        <form>
          <Grid
            container
            sx={{
              justifyContent: "center",
              marginTop: "5rem",
              marginLeft: "auto",
              marginRight: "auto",
            }}
            lg={10}
            md={7}
            sm={12}
            xs={12}
          >
            <Grid
              sx={{
                textAlign: "center",
              }}
            >
              <Grid>
                <Typography>Section 3 / 5: Company Details</Typography>
                <img src={pad} alt="paper" />
              </Grid>

              <img src={house} alt="bird" className={classes.house} />
              <Grid conatiner className={classes.gifCard}>
                <Typography>
                  Great, that's the property details done. Now tell us about the company we will be lending to?
                </Typography>
              </Grid>
              {props.questions
                .filter((item) => item.type === "mortgage")
                .map((x) => (
                  <>
                    <Grid
                      container
                      item
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        marginBottom: "2rem",
                        marginTop: "2rem",
                      }}
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid
                        className={classes.secGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "start",
                          alignItems: "center",
                        }}
                      >
                        <Typography>{x.question}</Typography>
                        <img
                          src={infoimg}
                          alt="info"
                          style={{ marginLeft: "2rem" }}
                          width="20px"
                        />
                      </Grid>
                    </Grid>
                    {/*  */}
                    {x.questionType === "singleInputField" ? (
                      <Grid
                        container
                        className={classes.thirdGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <TextField
                          label=""
                          placeholder={x.placeHolder}
                          size="small"
                          type="text"
                          name="firstName"
                          value={props.state.companyName}
                          onClick={() => props.inputFieldClicked()}
                          onChange={(e) =>
                            props.handleChange(
                              e,

                              x.id
                            )
                          }
                          className={classes.mortgage_box}
                        />
                      </Grid>
                    ): null}
                  </>
                ))}
            </Grid>
          </Grid>
        </form>
      </Container>
    </>
  );
};

export default Mortgage;
