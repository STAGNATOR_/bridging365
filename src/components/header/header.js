import React, { useState } from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";
import ProgressBars from "../sidebar/component/progressbar";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Grid,
  Button,
  Drawer,
  List,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import DehazeIcon from "@mui/icons-material/Dehaze";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import classes from "./header.module.css";
import logo from "../../Assets/header.png";
import pad from "../../Assets/pad.png";
import profile from "../../Assets/user.png";
import bank from "../../Assets/green.png";
import pound from "../../Assets/euro.png";
import home from "../../Assets/home.png";

const Header = () => {
  const navigate = useNavigate();
  const SidebarData = [
    {
      icon: pound,
      name: "About the Loan",
      href: "/outgoings",
    },
    {
      icon: bank,
      name: "About the Security",
      href: "/income",
    },
    {
      icon: pad,
      name: "About the Company",
      href: "/mortgage",
    },
    {
      icon: profile,
      name: "About the Director",
      href: "/about",
    },

    {
      icon: home,
      name: "Additional Information",
      href: "/property",
    },
  ];

  const [state, setState] = useState({
    left: false,
  });

  const [anchorEl, setAnchorEl] = useState(null);

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      // sx={{ width: 660, heigth: "100vh" }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
      className={classes.nav_main}
    >
      <List>
        <Grid lg={6} md={8} sm={5} xs={4} className={classes.d_flex_user}>
          <img src={logo} width="70px" className={classes.toggle_button} />
          <DehazeIcon
          // onClick={toggleDrawer("left", true)}
          // className={classes.toggle_button}
          />
        </Grid>
        {/* <KeyboardArrowLeftIcon
          onClick={toggleDrawer(anchor, false)}
          style={{
            marginTop: "20px",
            marginLeft: "140px",
          }}
        /> */}
        <Box className={classes.sidebar_main_mobile}>
          <Box
            className="logo-name-mobile cursor-pointer"
            onClick={() => navigate("/")}
          >
            <img src={logo} alt="" width="100px" className="image-mobile" />
          </Box>
          <Box className={classes.menu_mobile}>
            {SidebarData.map((x, i) => (
              <Grid
                className={classes.menu_item_mobile}
                onClick={() => navigate(x.link)}
                key={i}
              >
                <AnchorLink href={x.href}>
                  <img src={x.icon} alt="" className={classes.icone} />
                </AnchorLink>

                <span className={classes.menu_name}>
                  {" "}
                  <Grid>
                    <AnchorLink href={x.href} className={classes.text_h4}>
                      <Typography>{x.name}</Typography>
                    </AnchorLink>
                    <ProgressBars />
                  </Grid>
                </span>
              </Grid>
            ))}
          </Box>
          <Box sx={{ marginTop: "7rem", marginLeft: "4rem" }}>
            {/* <div className="socialmedia-icone"> */}
            <Grid>
              <Button variant="outlined" className={classes.button_save}>
                Save For Later
              </Button>
            </Grid>
            <Grid>
              <Button className={classes.button_Certificate}>
                Get Certificate
              </Button>
            </Grid>
            {/* </div> */}
          </Box>
        </Box>
      </List>
    </Box>
  );

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  //   const handleLogout = () => {
  //     console.log("111111");
  //     setAnchorEl(null);
  //     dispatch(UserLogoutFunction());
  //     navigate("/");
  //   };

  //   const gotoPage = () => {
  //     navigate("/user-profile");
  //     handleClose();
  //   };

  return (
    <Box>
      <Box>
        <Box className={classes.d_flex_user}>
          <img src={logo} width="10%" className={classes.toggle_button} />
          <DehazeIcon
            onClick={toggleDrawer("left", true)}
            className={classes.toggle_button}
          />

          <Drawer
            anchor={"left"}
            open={state["left"]}
            onClose={toggleDrawer("left", false)}
          >
            <Box className={classes.drawer}> {list("left")}</Box>
          </Drawer>
        </Box>
      </Box>
    </Box>
  );
};

export default Header;
