import { useState } from "react";
import { Container, Grid, Typography, TextField, Box } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import infoimg from "../../Assets/info.png";
import user from "../../Assets/user.png";
import trowel from "../../Assets/trowel.gif";
import DateAdapter from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import MuiPhoneNumber from "material-ui-phone-number";
import classes from "./about.module.css";

const About = (props) => {
  const [birthDay, setBirthDay] = useState(new Date());
  return (
    <>
      <LocalizationProvider dateAdapter={DateAdapter}>
        <Container>
          <form>
            <Grid
              container
              sx={{
                justifyContent: "center",
                marginTop: "5rem",
                marginLeft: "auto",
                marginRight: "auto",
              }}
              lg={10}
              md={7}
              sm={12}
              xs={12}
            >
              <Grid sx={{ textAlign: "center" }}>
                <Grid>
                  <Typography>Section 4 / 5: Directors Details</Typography>
                  <img src={user} alt="paper" />
                </Grid>

                <img src={trowel} alt="bird" className={classes.house} />
                <Grid conatiner className={classes.gifCard}>
                  <Typography>
                  We're getting close to the end. Please tell us about yourself...
                  </Typography>
                </Grid>
                {props.questions
                  .filter((item) => item.type === "director")
                  .map((x) => (
                    <>
                      <Grid
                        sx={{
                          display: "flex",
                          justifyContent: "center",
                          marginBottom: "2rem",
                          marginTop: "2rem",
                        }}
                        lg={12}
                        md={12}
                        sm={12}
                        xs={12}
                      >
                        <Grid
                          className={classes.secGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "start",
                            alignItems: "center",
                          }}
                        >
                          <Typography>{x.question}</Typography>
                          <img
                            src={infoimg}
                            alt="info"
                            style={{ marginLeft: "2rem" }}
                            width="20px"
                          />
                        </Grid>
                      </Grid>

                      {x.questionType === "singleInputField" ? (
                        <>
                          {/* Input Grid start */}
                          <Grid
                            container
                            className={classes.thirdGifCard}
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <TextField
                              label=""
                              id="fullWidth"
                              size="small"
                              placeholder={x.placeHolder}
                              onClick={() => props.inputFieldClicked()}
                              onChange={(e) =>
                                props.handleChange(
                                  e,

                                  x.id
                                )
                              }
                              sx={{
                                backgroundColor: "#fff",
                                // marginBottom: "2rem",
                              }}
                              className={classes.about_mortgage_box}
                            />
                          </Grid>
                          {/* Input Grid End */}
                        </>
                      ) : x.questionType === "calendar" ? (
                        <>
                          {/* DOB Grid Start */}
                          <Grid
                            className={classes.thirdGifCard}
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <DatePicker
                              disableFuture
                              label=""
                              openTo="year"
                              views={["year", "month", "day"]}
                              value={x.answer}
                              // onChange={(newBirthDay) => {
                              //   setValue(newBirthDay);
                              // }}
                              onClick={() => props.inputFieldClicked()}
                              onChange={(value) =>
                                props.handlePhoneChange(value, "calendar")
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  sx={{
                                    width: "70%",
                                    backgroundColor: "white",
                                    borderRadius: "5px",
                                  }}
                                />
                              )}
                              sx={{ width: "70%", color: "black" }}
                            />
                          </Grid>
                          {/* DOB GRID END */}
                        </>
                      ) : x.questionType === "boolean" ? (
                        <>
                          {/* Radio Grid Start */}
                          <Grid
                            className={classes.thirdGifCard}
                            sx={{
                              display: "flex",
                              justifyContent: "start",
                              alignItems: "center",
                            }}
                          >
                            <FormControl>
                              <RadioGroup
                                aria-labelledby="demo-controlled-radio-buttons-group"
                                name="controlled-radio-buttons-group"
                                value={props.value}
                                onClick={() => props.inputFieldClicked()}
                                onChange={(e) =>
                                  props.handleChange(
                                    e,

                                    x.id
                                  )
                                }
                              >
                                {x.answerOptions.map((y) => (
                                  <FormControlLabel
                                    value={y}
                                    control={
                                      <Radio
                                        sx={{
                                          color: "#ebbe34",

                                          "&.Mui-checked": {
                                            color: "#ebbe34",
                                          },
                                        }}
                                      />
                                    }
                                    label={y}
                                  />
                                ))}
                              </RadioGroup>
                            </FormControl>
                          </Grid>
                          {/* Radio Grid End */}
                        </>
                      ) : (
                        <>
                          {/* Phone Grid Start */}
                          <Grid
                            className={classes.thirdGifCard}
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <MuiPhoneNumber
                              variant="outlined"
                              defaultCountry={"gb"}
                              onChange={(value) =>
                                props.handlePhoneChange(value, "phone")
                              }
                              sx={{
                                width: "70%",
                                backgroundColor: "white",
                                borderRadius: "5px",
                              }}
                            />
                          </Grid>
                          {/* Phone Grid End */}
                        </>
                      )}
                    </>
                  ))}
              </Grid>
            </Grid>
          </form>
        </Container>
      </LocalizationProvider>
    </>
  );
};

export default About;
