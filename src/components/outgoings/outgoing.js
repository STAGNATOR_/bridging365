import { useState } from "react";
import { Container, Grid, Typography, TextField, Box } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";

import pound from "../../Assets/euro.png";
import key from "../../Assets/key.gif";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import infoimg from "../../Assets/info.png";

import InputLabel from "@mui/material/InputLabel";

import classes from "./outgoing.module.css";

const Outgoings = (props) => {
  const [age, setAge] = useState("");
  // const [obj, setObj] = useState("");

  const handleChange =(event) =>{
    setAge(event.target.age)
  }

  return (
    <>
      <Container>
        <form>
          <Grid
            container
            sx={{
              justifyContent: "center",
              marginTop: "5rem",
              marginLeft: "auto",
              marginRight: "auto",
            }}
            lg={10}
            md={7}
            sm={12}
            xs={12}
          >
            <Grid sx={{ textAlign: "center" }}>
              <Grid>
                <Typography>Section 1 / 5: Bridging Loan Details</Typography>
                <img src={pound} alt="paper" />
              </Grid>

              <img src={key} alt="bird" className={classes.house} />
              <Grid conatiner className={classes.gifCard}>
                <Typography>
                We'll be asking you about the loan, and a few details about you, your company and the property. This should take no longer than 10 minutes, and you can save it at any stage.
                             </Typography>
              </Grid>
              {props.questions
                .filter((item) => item.type === "loan")
                .map((x) => (
                  <>
                    <Grid
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        marginBottom: "2rem",
                        marginTop: "2rem",
                      }}
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid
                        className={classes.secGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Typography>{x.question}</Typography>
                        <img
                          src={infoimg}
                          alt="info"
                          style={{ marginLeft: "2rem" }}
                          width="20px"
                        />
                      </Grid>
                    </Grid>
                    {x.questionType === "dropdown" ? (
                      <>
                        <Grid
                          container
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <FormControl className={classes.about_mortgage_box}>
                          <InputLabel id="demo-simple-select-label">Purpose</InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={age}
                              label="Age"
                              onChange={(e) =>
                                props.handleOutingRadioChange(e, x.id)
                              }
                            >
                              <MenuItem value="Purchase">Purchase</MenuItem>
                              <MenuItem value="Refinance">Refinance</MenuItem>
                              <MenuItem value="Capital raise- home improvements">
                                Capital raise- home improvements
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                                Stop a repossession
                              </MenuItem>
                            </Select>
                          </FormControl>
                          
                        </Grid>
                      </>
                    ) : x.questionType === "singleInput" ? (
                      <>
                        <Grid
                          container
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <TextField
                            label=""
                            id="fullWidth"
                            size="small"
                            onClick={() => props.inputFieldClicked()}
                            onChange={(e) =>
                              props.handleChange(
                                e,

                                x.id
                              )
                            }
                            sx={{
                              backgroundColor: "#fff",
                              // marginBottom: "2rem",
                            }}
                            className={classes.about_mortgage_box}
                          />
                        </Grid>
                      </>
                    )
                     : x.questionType === "boolean" ? (
                      <>
                        <Grid
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "start",
                            alignItems: "center",
                          }}
                        >
                          <FormControl>
                            <RadioGroup
                              placeholder="please select from below"
                              aria-labelledby="demo-controlled-radio-buttons-group"
                              name="controlled-radio-buttons-group"
                              value={props.value}
                              // onClick={() => props.inputFieldClicked()}
                              onChange={(e) =>
                                props.handleChange(
                                  e,

                                  x.id
                                )
                              }
                            >
                              {x.answerOptions.map((y) => (
                                <FormControlLabel
                                  value={y}
                                  control={
                                    <Radio
                                      sx={{
                                        color: "#ebbe34",

                                        "&.Mui-checked": {
                                          color: "#ebbe34",
                                        },
                                      }}
                                    />
                                  }
                                  label={y}
                                />
                              ))}
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                      </>
                    ) : null}
                  </>
                ))}
              {props.dropValue === "Refinance"
                ? props.questions
                    .filter((item) => item.type === "loan1")
                    .map((a) => (
                      <>
                        <Grid
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            marginBottom: "2rem",
                            marginTop: "2rem",
                          }}
                          lg={12}
                          md={12}
                          sm={12}
                          xs={12}
                        >
                          <Grid
                            className={classes.secGifCard}
                            sx={{
                              display: "flex",
                              justifyContent: "start",
                              alignItems: "center",
                            }}
                          >
                            <Typography>{a.question}</Typography>
                            <img
                              src={infoimg}
                              alt="info"
                              style={{ marginLeft: "2rem" }}
                              width="20px"
                            />
                          </Grid>
                        </Grid>

                        <Grid
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "start",
                            alignItems: "center",
                          }}
                        >
                          <FormControl>
                            <RadioGroup
                              aria-labelledby="demo-controlled-radio-buttons-group"
                              name="controlled-radio-buttons-group"
                              value={props.value}
                              // onClick={() => props.inputFieldClicked()}
                              onChange={(e) =>
                                props.handleChange(
                                  e,

                                  a.id
                                )
                              }
                            >
                              {a.answerOptions.map((n) => (
                                <FormControlLabel
                                  value={n}
                                  control={
                                    <Radio
                                      sx={{
                                        color: "#ebbe34",

                                        "&.Mui-checked": {
                                          color: "#ebbe34",
                                        },
                                      }}
                                    />
                                  }
                                  label={n}
                                />
                              ))}
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                      </>
                    ))
                : null}
              {props.dropValue === "Stop a repossession"
                ? props.questions
                    .filter((item) => item.type === "loan3")
                    .map((a) => (
                      <>
                        <Grid
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            marginBottom: "2rem",
                            marginTop: "2rem",
                          }}
                          lg={12}
                          md={12}
                          sm={12}
                          xs={12}
                        >
                          <Grid
                            className={classes.secGifCard}
                            sx={{
                              display: "flex",
                              justifyContent: "start",
                              alignItems: "center",
                            }}
                          >
                            <Typography>{a.question}</Typography>
                            <img
                              src={infoimg}
                              alt="info"
                              style={{ marginLeft: "2rem" }}
                              width="20px"
                            />
                          </Grid>
                        </Grid>

                        <Grid
                          container
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <FormControl className={classes.about_mortgage_box}>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              // value={props.value}
                              label=""
                              onChange={(e) =>
                                props.handleChange(
                                  e,

                                  a.id
                                )
                              }
                            >
                              <MenuItem value={"under 48 hours"}>
                                under 48 hours
                              </MenuItem>
                              <MenuItem value={"under a week"}>
                                under a week
                              </MenuItem>
                              <MenuItem value={"under a month"}>
                                under a month
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>
                      </>
                    ))
                : null}
              {props.dropValue === "Capital raise- home improvements" ? (
                <>
                  <Grid
                    container
                    className={classes.thirdGifCard}
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      marginTop: "2rem",
                    }}
                  >
                    <TextField
                      label=""
                      id="fullWidth"
                      size="small"
                      onClick={() => props.inputFieldClicked()}
                      onChange={(e) =>
                        props.handleChange(
                          e,

                          props.questions.id
                        )
                      }
                      sx={{
                        backgroundColor: "#fff",
                        // marginBottom: "2rem",
                      }}
                      className={classes.about_mortgage_box}
                    />
                  </Grid>
                </>
              ) : null}
              {props.questions
                .filter((item) => item.type === "loan2")
                .map((x) => (
                  <>
                    <Grid
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        marginBottom: "2rem",
                        marginTop: "2rem",
                      }}
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid
                        className={classes.secGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Typography>{x.question}</Typography>
                        <img
                          src={infoimg}
                          alt="info"
                          style={{ marginLeft: "2rem" }}
                          width="20px"
                        />
                      </Grid>
                    </Grid>
                    {x.questionType === "dropdown" ? (
                      <>
                        <Grid
                          container
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <FormControl className={classes.about_mortgage_box}>
                          <InputLabel id="demo-simple-select-label">Borrow money</InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={age}
                              label="month"
                              // onOpen={(e) => consoleHandler(e)}
                              onChange={(e) => props.handleChange(e, x.id)}
                            >
                              <MenuItem value="Purchase"></MenuItem>
                              <MenuItem value="Refinance">1 Month</MenuItem>
                              <MenuItem value="Capital raise- home improvements">
                              2 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              3 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              4 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              5 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              6 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              7 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              8 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              9 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              10 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              11 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              12 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              13 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              14 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              15 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              16 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              17 Month
                              </MenuItem>
                              <MenuItem value="Stop a repossession">
                              18 Month
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>
                      </>
                    ) : x.questionType === "singleInput" ? (
                      <>
                        <Grid
                          container
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <TextField
                            label=""
                            id="fullWidth"
                            size="small"
                            placeholder={x.placeHolder}
                            onClick={() => props.inputFieldClicked()}
                            onChange={(e) =>
                              props.handleChange(
                                e,

                                x.id
                              )
                            }
                            sx={{
                              backgroundColor: "#fff",
                              // marginBottom: "2rem",
                            }}
                            className={classes.about_mortgage_box}
                          />
                        </Grid>
                      </>
                    ) : x.questionType === "boolean" ? (
                      <>
                        <Grid
                          className={classes.thirdGifCard}
                          sx={{
                            display: "flex",
                            justifyContent: "start",
                            alignItems: "center",
                          }}
                        >
                          <FormControl>
                            <RadioGroup
                              placeholder="please select from below"
                              aria-labelledby="demo-controlled-radio-buttons-group"
                              name="controlled-radio-buttons-group"
                              value={props.value}
                              // onClick={() => props.inputFieldClicked()}
                              onChange={(e) =>
                                props.handleChange(
                                  e,

                                  x.id
                                )
                              }
                            >
                              {x.answerOptions.map((y) => (
                                <FormControlLabel
                                  value={y}
                                  control={
                                    <Radio
                                      sx={{
                                        color: "#ebbe34",

                                        "&.Mui-checked": {
                                          color: "#ebbe34",
                                        },
                                      }}
                                    />
                                  }
                                  label={y}
                                />
                              ))}
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                      </>
                    ) : null}
                  </>
                ))}
            </Grid>
          </Grid>
        </form>
      </Container>
    </>
  );
};

export default Outgoings;
