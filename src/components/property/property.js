import { useState } from "react";
import { Container, Grid, Typography, TextField, Box } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import infoimg from "../../Assets/info.png";
import flower from "../../Assets/flower.gif";
import home from "../../Assets/home.png";

import classes from "./property.module.css";

const Property = (props) => {
  const [value, setValue] = useState("");
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  return (
    <>
      <Container>
        <form>
          <Grid
            container
            sx={{
              justifyContent: "center",
              marginTop: "5rem",
              marginLeft: "auto",
              marginRight: "auto",
            }}
            lg={10}
            md={7}
            sm={12}
            xs={12}
          >
            <Grid sx={{ textAlign: "center" }}>
              <Grid>
                <Typography>Section 5 / 5: Anything else?</Typography>
                <img src={home} alt="paper" />
              </Grid>

              <img src={flower} alt="bird" className={classes.flower} />
              <Grid conatiner className={classes.gifCard}>
                <Typography>
                  For this last section, here's your chance to tell us anything else you think we need to know about you or the application.
                </Typography>
              </Grid>
              {props.questions
                .filter((item) => item.type === "property")
                .map((x) => (
                  <>
                    <Grid
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        marginBottom: "2rem",
                        marginTop: "2rem",
                      }}
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid
                        className={classes.secGifCard}
                        sx={{
                          display: "flex",
                          justifyContent: "start",
                          alignItems: "center",
                        }}
                      >
                        <Typography>{x.question}</Typography>
                        <img
                          src={infoimg}
                          alt="info"
                          style={{ marginLeft: "2rem" }}
                          width="20px"
                        />
                      </Grid>
                    </Grid>

                    <Grid
                      container
                      className={classes.thirdGifCard}
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <TextField
                        size="small"
                        label=""
                        id="fullWidth"
                        placeholder={x.placeHolder}
                        className={classes.about_mortgage_box}
                      />
                    </Grid>
                  </>
                ))}
            </Grid>
          </Grid>
        </form>
      </Container>
    </>
  );
};

export default Property;
