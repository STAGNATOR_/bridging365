import classes from "./sidebar.module.css";
import React,{useEffect} from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { Grid, Box, Button, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

import logo from "../../Assets/header.png";

import pad from "../../Assets/pad.png";
import profile from "../../Assets/user.png";
import bank from "../../Assets/green.png";
import pound from "../../Assets/euro.png";
import home from "../../Assets/home.png";
import ProgressBars from "./component/progressbar";

const Sidebar = (props) => {
  const handleSubmit = () => {
    console.log("questions", props.questions);
  };
  const navigate = useNavigate();
  const SidebarData = [
    {
      icon: pound,
      name: "Bridging Loan Details",
      href: "/outgoings",
      // link: "/media",
    },
    {
      icon: bank,
      name: "Security Details",
      href: "/income",
      // link: "/media",
    },
    {
      icon: pad,
      name: "Company Details",
      href: "/mortgage",
      // link: "/",
    },
    {
      icon: profile,
      name: "Directors Details",
      href: "/about",
      // link: "preview-course",
    },

    {
      icon: home,
      name: "Additional Information",
      href: "/property",
      // link: "/media",
    },
  ];

  return (
    <div>
      <Grid container style={{ heigth: "100vh" }}>
        <Grid
          className={classes.sidebar_main}
          sx={{ borderRight: "1px solid #ccc" }}
        >
          <Grid
            className="logo-name cursor-pointer"
            // onClick={() => navigate("/")}
            sx={{ margin: "2rem" }}
          >
            <img
              src={logo}
              alt=""
              className="image"
              width="150px"
              href="/mortgage"
            />
          </Grid>
          <Grid className="menu">
            {SidebarData.map((x, i) => (
              <Grid
                className={classes.menu_item}
                onClick={() => navigate(x.link)}
                key={i}
              >
                <Grid>
                  <AnchorLink href={x.href} className={classes.text_h4}>
                    <Typography>{x.name}</Typography>
                  </AnchorLink>
                  <ProgressBars />
                </Grid>
                <span className={classes.menu_name}>
                  {" "}
                  <AnchorLink href={x.href}>
                    <img src={x.icon} alt="" className={classes.icone} />
                  </AnchorLink>
                </span>
              </Grid>
            ))}
          </Grid>

          <Box style={{ position: "relative", bottom: "0px", margin: "2rem" }}>
            <div className="socialmedia-icone">
              <Grid>
                <Button variant="outlined" className={classes.button_save}>
                  Save For Later
                </Button>
              </Grid>
              <Grid>
                <Button
                  className={classes.button_Certificate}
                  onClick={props.handleSubmit}
                >
                  Get Certificate
                </Button>
              </Grid>
            </div>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Sidebar;
