import { Grid, Typography } from "@mui/material";
import h2img from "../../Assets/h2.png";
import lcd from "../../Assets/lcd.png";
import letter from "../../Assets/letter.png";
import classes from "./certificate.module.css";
import Image from "../../Assets/paper.svg";
import EuroIcon from '@mui/icons-material/Euro';
const Certificate = () => {
  return (
    <>
      <Grid
        container
        sx={{
          marginTop: "2rem",
          paddingTop: "2rem",
          paddingRight: "2rem",
          //   marginLeft: "2rem",

          borderLeft: "1px solid #ccc",
          height: "100%",
        }}
        lg={12}
        md={12}
        sm={12}
        xs={12}
        className={classes.Certificate_main}
      >
        <Grid container sx={{ padding: " 1rem" ,display:"flex", justifyContent:"center" }}>
          <Typography className={classes.main_text}>
           Your Offer in Principle 
          </Typography>
        </Grid>
        <Grid container sx={{ padding: " 10px"  ,display:"flex", justifyContent:"center"  , textAlign:"center"}}>
          <Typography className={classes.text1}>
           Based on our calculations so far:
          </Typography>
        </Grid>
        <Grid container sx={{ padding: " 10px" ,display:"flex", justifyContent:"center" }}>
          <Typography className={classes.text1}>
           You want to borrow up to:
          </Typography>
        </Grid>
        <Grid container sx={{display:"flex", justifyContent:"center"}} >
         <EuroIcon sx={{color:"#e56a54" , fontSize:"30px"}} />
         <Typography sx={{color:"#e56a54", fontSize:"20px"}} >100,000</Typography>
        </Grid>
        <Grid container sx={{ padding: " 10px" ,display:"flex", justifyContent:"center" , textAlign:"center"  }}>
          <Typography className={classes.text1}>
           You may be able to borrow up to:
          </Typography>
        </Grid>
        <Grid container sx={{display:"flex", justifyContent:"center"}} >
         <EuroIcon sx={{color:"#e56a54" , fontSize:"30px"}} />
         <Typography sx={{color:"#e56a54", fontSize:"20px"}} >750,000</Typography>
        </Grid>
        <Grid container sx={{ display:"flex", justifyContent:"center"}}>
          <Typography className={classes.text1}>
           Over<span className={classes.month}>5</span>month
          </Typography>
        </Grid>
        <Grid container sx={{ display:"flex", justifyContent:"center" , textAlign:"center"  }}>
          <Typography className={classes.text1}>
           Interest rate 0.69%
          </Typography>
        </Grid>
      </Grid>
    </>
  );
};

export default Certificate;
